# XG Configuration Utility

Secure Configuration Storage and Retrieval Utility

Requirements:

- ```_CONFIG_DIR_``` must be defined globally and contain a path to a directory on the server where .ini files live
- PHP must be able to read the ```_CONFIG_DIR_``` directory
- PHP 7+
- A file name ```salt``` must be present in the ```_CONFIG_DIR_``` and contain a random alphanumeric string. This value will be used in encryption to salt the hash.

### INI Format
```
; Databse configuration
[database]
name = "my_database_name"
user = "db_user"
password = "~encrypted[78d9das9879as7d9as7d9a]"
host = "localhost"

[AWS]
key = "~encrypted[897fs8df79sdfsd9f7sdf9sdf879s7fsd8f7sdfs9d7]"
secret = "~encrypted[sfd7sd9f87s9df87sd9fsd9f87sd9f7sd9f8sf7s9f7s9dfs9df7s9f7s]"

; My Custom Config Keys
[custom]
test = "test"
encryption_test = "~encrypted[w3i3n3x3d2k363q3s3d3a3]"

```

###Retreival
```php
// Decryption is automatic
$aws_key = \XGWeb\Config::instance()->getGroup('AWS')->getKey('key');
```
### Encrypted Key Creation
Execute the following bash script:

```bash
$ php vendor/xgweb/config/src/encrypt.php /my/_config_dir_/path/config/ tester hello

Copy and paste the following into your config file:
tester = ~encrypted[g4e4c4l4k4]

```
