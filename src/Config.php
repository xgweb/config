<?php
namespace XGWeb;
use ArrayObject;

/**
 * Secure Configuration Storage and Retrieval Utility
 */
class Config {
    const DEFAULT_FILE_NAME = 'main';
    private $_config;
    private $_group;

    function __construct($file_name = null)
    {
        $file_path = _CONFIG_DIR_ . ($file_name ?? _CONFIG_FILE_ ?? self::DEFAULT_FILE_NAME) . '.ini';
        if (!file_exists($file_path)) {
            throw new \Exception('Config file not found. [' . $file_path . ']');
        }

        $this->_config = new ArrayObject(parse_ini_file($file_path, true));
    }

    public static function instance(string $file_name = null): self
    {
        return new self($file_name);
    }

    private static function _getSalt()
    {
        return file_get_contents(_CONFIG_DIR_ . 'salt');
    }

    public static function buildEncryptedKey($value)
    {
        return '~encrypted[' . self::_encode($value) . ']';
    }

    public function getGroup(string $group): ArrayObject
    {
        $this->setGroup($group);
        $return = [];
        foreach ($this->_group as $key => $value) {
            $return[$key] = $this->getKey($key);
        }
        
        return new ArrayObject($return);
    }

    public function setGroup(string $group): self
    {
        $result = $this->_config->offsetGet($group);
        $this->_group = new ArrayObject();
        if (!$result) {
            return $this;
        }

        if (is_array($result)) {
            $this->_group = new ArrayObject($result);
            return $this;
        }

        return $this;
    }

    public function getKey(string $key)
    {
        $value = $this->_group->offsetGet($key);
        if ($value === null) {
            return;
        }

        $regex = "/~json\[(.*?)\]/";
        preg_match_all($regex, $value, $matches);

        if (isset($matches[1][0])) {
            return json_decode($matches[1][0] .']', true);
        }

        $regex = "/~encrypted\[(.*?)\]/";
        preg_match_all($regex, $value, $matches);
        if (!isset($matches[1][0])) {
            return $value;
        }

        $value = self::_decode($matches[1][0]);
        if (!$value) {
            return;
        }

        return $value;
    }

    private static function _encode($string) {

        $j = 0;
        $key = sha1(self::_getSalt());
        $strLen = strlen($string);
        $keyLen = strlen($key);
        $hash = '';

        for ($i = 0; $i < $strLen; $i++) {
            $ordStr = ord(substr($string, $i, 1));
            if ($j == $keyLen) {
                $j = 0;
            }

            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $hash .= strrev(base_convert(dechex($ordStr + $ordKey), 16, 36));
        }
        return $hash;
    }

    private static function _decode($string) {
        $j = 0;
        $hash = '';
        $key = sha1(self::_getSalt());
        $strLen = strlen($string);
        $keyLen = strlen($key);
        for ($i = 0; $i < $strLen; $i += 2) {
            $ordStr = hexdec(base_convert(strrev(substr($string, $i, 2)), 36, 16));
            if ($j == $keyLen) { $j = 0;
            }

            $ordKey = ord(substr($key, $j, 1));
            $j++;
            $hash .= chr($ordStr - $ordKey);
        }
        return $hash;
    }

}
